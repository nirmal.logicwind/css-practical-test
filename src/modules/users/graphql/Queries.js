import { gql } from '@apollo/client';

// eslint-disable-next-line import/prefer-default-export
export const GET_USERS = gql`
  query adminUsers($filter: UserFilter!) {
    adminUsers(filter: $filter) {
      count
      users {
        id
        firstName
        lastName
        email
        phoneNo
        roles
        profileImage
        isActive
      }
    }
  }
`;

export const GET_PROFILE = gql`
  query getProfile {
    getProfile {
      id
      firstName
      lastName
      email
      phoneNo
      roles
      profileImage
      isActive
      tenantId
    }
  }
`;
